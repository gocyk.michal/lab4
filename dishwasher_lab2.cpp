#include <iostream>
#include <fstream>
#include <vector>
#include <conio.h>
#include <time.h>

using namespace std;
time_t now, later;
 
void sleep(int delay)
{
 now=time(NULL);
 later=now+delay;
 while(now<=later)now=time(NULL);
}


class programs
{
	public:
	int id;
	string names;
	int time;
	int temperature;
	int power;
	
	programs( int xid, string xnames, int xtime, int xtemperature, int xpower)
	{
		id = xid;
		names = xnames;
		time = xtime;
		temperature = xtemperature;
		power = xpower;
	}
			
	void print_data()
	{
		cout<<id<<"."<<names<< " - time: "<< time<<" [min];"<<" temperatue: "<<temperature<<" [C];"<< " power consumption: " << power<<" [W];"<< "\n";
	}	
};

void read(fstream & program, vector<programs> & tab_programs)
{
	int ID;
	string NAME;
	int TIME;
	int TEMP;
	int POW;
	program.open("programs_2.txt", ios::in);

	for( int i = 0; i<4; i++)
	{
		program>>ID;
		program>>NAME;
		program>>TIME;
		program>>TEMP;
		program>>POW;
		tab_programs.push_back(programs (ID,NAME, TIME, TEMP,POW));

	}
	program.close();
}

void print(vector<programs> & tab_programs)
{
	for( int i = 0; i<tab_programs.size(); i++)
	{
	tab_programs[i].print_data();
	}
}

main()
{
	fstream lab2;
	vector<programs> tab_programs;
	read(lab2, tab_programs);
	
	int a=0;
	int b;
	int button;
	do
	{
		menu:
		print(tab_programs);
		cout<<"Choose program: ";
		cin>>a;
		system("cls");
		cout<<"The chosen program is "<<tab_programs[a-1].names<< " - time: "<< tab_programs[a-1].time <<" [min];"<<" temperatue: "<<tab_programs[a-1].temperature <<" [C];"<< " power consumption: " << tab_programs[a-1].power<<" [W]"<<"."<<endl<<endl;
		cout<<"Press any key to start"<<endl;
		getch();
	}
	while(a<0 || a>6);

	a--;
	system("cls");
	cout<<"Running"<<endl;
	button = 0;

	do
	{
		if(kbhit())
		{	
		button = getch();
		}
			
		if( button == 112 )
		{
		cout<<tab_programs[a].time<<" minutes left."<<endl;
		
		tab_programs[a].time = tab_programs[a].time;
		cout<<"PAUSED"<< endl<<"press any key to resume"<<endl;
		sleep(0.5);
		system("cls");
		}
		
		else{
			cout<<tab_programs[a].time<<" minutes left"<<endl;
			cout<<"press p to pause"<<endl<<"press s to stop";
		
		tab_programs[a].time = 	tab_programs[a].time - 1;
		sleep(0.5);
		system("cls");
			}			
	}
		
	
	while(tab_programs[a].time > 0 && button != 115);

	cout<<"The washing has ended."<<endl;
	sleep(1.5);
	system("cls");

	goto menu;
	
	
return 0;
}

